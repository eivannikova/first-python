import sys
import re

for line in sys.stdin:
    words = re.split("\\W+", line)
    for word in words:
        if word:
            print(word, 1, sep=" ")
