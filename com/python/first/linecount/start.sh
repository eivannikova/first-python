#!/usr/bin/env bash
HADOOP_STREAMING_JAR="/opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar"
yarn jar $HADOOP_STREAMING_JAR \
    -files mapper.py,reducer.sh \
    -mapper 'python ./mapper.py' \
    -reducer './reducer.sh' \
    -numReduceTasks 1 \
    -input /data/wiki/my_articles/ecotourism \
    -output wc_mr_with_reducer
