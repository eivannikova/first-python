from pyspark import SparkConf, SparkContext
import re

sc = SparkContext(conf=SparkConf().setAppName("MyPythonApp").setMaster("local[2]"))


def parse_csv(line):
    try:
        text = line.strip()
    except ValueError:
        return []
    text = re.sub("^\\W+|\\W+$", "", text, flags=re.UNICODE)
    words = re.split("\\W*,\\W*", text, flags=re.UNICODE)
    return words


wiki = sc.textFile("1.csv", 2).map(parse_csv)
result = wiki.take(wiki.count())

for words in result:
    for word in words:
        print(word)
