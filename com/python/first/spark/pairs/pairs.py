from pyspark import SparkConf, SparkContext
import re

sc = SparkContext(conf=SparkConf().setAppName("PythonPairs").setMaster("local[2]"))


def parse(line):
    try:
        text = line.strip()
    except ValueError:
        return []
    text = re.sub("^\\W+|\\W+$", "", text, flags=re.UNICODE)
    words = re.split("\\W*\\s+\\W*", text, flags=re.UNICODE)
    return words


def pairs(text, first_word):
    c = []
    for i, word in enumerate(text[:-1]):
        if word == first_word:
            current_word = f'{word}_{text[i + 1]}'
            c.append((current_word, 1))
    return c


result_parsed = sc.textFile("philosophy.txt", minPartitions=2).map(parse)
result_lower = result_parsed.map(lambda words: [word.lower() for word in words])
result_pairs = result_lower.flatMap(lambda words: pairs(words, "one"))
result_filtered = result_pairs.filter(lambda words: words != [])
result_aggregated = result_filtered.reduceByKey(lambda x, y: x + y, numPartitions=4)
result_sorted = result_aggregated.sortByKey()

for pair, count in result_sorted.collect():
    print(pair, count, sep="\t")
