from collections import Counter
from pyspark import SparkConf, SparkContext
import re

sc = SparkContext(conf=SparkConf().setAppName("PythonPairs").setMaster("local[2]"))
words_counter = Counter()
pairs_counter = Counter()


def parse(line):
    try:
        text = line.strip()
    except ValueError:
        return []
    text = re.sub("^\\W+|\\W+$", "", text, flags=re.UNICODE)
    words = re.split("\\W*\\s+\\W*", text, flags=re.UNICODE)
    return words


def word_pair(text):
    c = []
    for i, word in enumerate(text[:-1]):
        current_pair = f'{word} {text[i + 1]}'
        c.append((current_pair, 1))
    return c


stop_words_file = sc.textFile("stop_words.txt")
stop_words_lower = stop_words_file.map(lambda word: word.lower())
stop_words = stop_words_lower.take(stop_words_file.count())

all_words_file = sc.textFile("philosophy.txt", minPartitions=2).map(parse)
all_words_lower = all_words_file.map(lambda words: [word.lower() for word in words])
all_words = all_words_lower.map(lambda words: [word for word in words if word not in stop_words])

word_pairs = all_words.flatMap(lambda words: word_pair(words))
word_pairs_aggregated = word_pairs.reduceByKey(lambda x, y: x + y)
word_pairs_sorted = word_pairs_aggregated.sortBy(lambda x: -x[1])

print(word_pairs_sorted.collect())
