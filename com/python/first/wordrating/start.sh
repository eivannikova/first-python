#!/usr/bin/env bash

OUT_DIR="word_rating"
NUM_REDUCERS=2
HADOOP_STREAMING_JAR="/opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar"

hdfs dfs -rm -r -skipTrash ${OUT_DIR} > /dev/null

yarn jar $HADOOP_STREAMING_JAR \
    -D mapreduce.job.reduces=${NUM_REDUCERS} \
    -D mapreduce.map.output.key.field.separator=\t \
    -files mapper.py,reducer.py \
    -mapper 'python mapper.py' \
    -reducer 'python reducer.py' \
    -combiner 'python reducer.py' \
    -input /data/wiki/my_articles \
    -output ${OUT_DIR}

OUT_DIR_2="word_rating_res"
NUM_REDUCERS=1

hdfs dfs -rm -r -skipTrash ${OUT_DIR_2} > /dev/null

yarn jar $HADOOP_STREAMING_JAR \
    -D mapreduce.job.reduces=${NUM_REDUCERS} \
    -D mapreduce.map.output.key.field.separator=\t \
    -D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
    -D mapreduce.partition.keycomparator.options=-k1,1nr \
    -files mapper_res.py,reducer_res.py \
    -mapper 'python mapper_res.py' \
    -reducer 'python reducer_res.py' \
    -input ${OUT_DIR} \
    -output ${OUT_DIR_2}