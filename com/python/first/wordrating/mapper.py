from __future__ import print_function
import sys
import re
from collections import Counter

for line in sys.stdin:
    line = re.sub("^\\W+|\\W+$", "", line, flags=re.UNICODE)
    words = re.split("\\W*\\s+\\W*", line, flags=re.UNICODE)
    counts = Counter(words)

    for word in counts:
        print(word.lower(), counts[word], sep="\t")
