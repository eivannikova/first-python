#### **DDL: Create Tables (task description):**

Create your own database and 'use' it. Create external table 'posts_sample_external' over the sample dataset with posts
in '/data/stackexchange1000' directory. Create managed table 'posts_sample' and populate with the data from the external table.
'Posts_sample' table should be partitioned by year and by month of post creation.
Provide output of query which selects lines number per each partition in the format:

    year <tab> month <tab> lines count

where year in YYYY format and month in YYYY-MM format.