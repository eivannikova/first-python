#### **DML: Find Most Popular Tags (task description):**

Compare most popular tags in year 2016 with tags in 2009. 
Tag popularity is the number of questions (post_type_id=1) with this tag. 
Output top 10 tags in format:

    tag <tab> rank in 2016 <tab> rank in 2009 <tab> tag popularity in 2016 <tab> tag popularity in 2009

For 'rank' in each year use rank() function. Order by 'rank in 2016'.
Use the table 'posts_sample' in the similar way to DDL task. 'Posts_sample' is partitioned by year and by month.